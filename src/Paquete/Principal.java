/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Paquete;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 *
 * @author Luis
 */
public class Principal {
    public static void main(String args[])
    {
 EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosAMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Collection<Lineaorden> lineaop;
        lineaop = em.createNamedQuery("Lineaorden.findAll").getResultList();
        Orden orden = em.find(Orden.class, 2);
        for (Lineaorden lo : lineaop) {
            if (!orden.getIdOrden().equals(lo.getOrden().getIdOrden())) {
                System.out.println("Numero de Orden: " + lo.getOrden().getIdOrden());
                System.out.println("Fecha: " + lo.getOrden().getFechaOrden());
            }
            System.out.println("Producto: " + lo.getProducto().getIdProducto());
            System.out.println("Cantidad: " + lo.getCantidad());
            System.out.println("Precio: " + lo.getProducto().getPrecio());
             orden = lo.getOrden();
        }
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        em.close();
        emf.close();       
    }
}
//    EntityManagerFactory emf1 = Persistence.createEntityManagerFactory("MuchosAMuchosPU");
//    EntityManager em = emf1.createEntityManager();
//    em.getTransaction().begin();
//    int x=250;
//    int y=300;
//    int z=240;
//    int a=210;
//    Float q = new Float(x);
//    Float w = new Float(y);
//    Float e = new Float(z);
//    Float r = new Float(a);
//    Date fecha = new Date();
//    
//    
//    Producto producto1 = new Producto();
//    producto1.setIdProducto(1);
//    producto1.setDescription("Playera Juventus 2013");
//    producto1.setPrecio(q);
//    
//    Producto producto2 = new Producto();
//    producto2.setIdProducto(2);
//    producto2.setDescription("Playera Mexico Mundial 2006");
//    producto2.setPrecio(w);
//   
//    Producto producto3 = new Producto();
//    producto3.setIdProducto(3);
//    producto3.setDescription("Playera Toluca 2014");
//    producto3.setPrecio(e);
//    
//    Producto producto4 = new Producto();
//    producto4.setIdProducto(4);
//    producto4.setDescription("Playera Borussia Dortmund 2014");
//    producto4.setPrecio(r);
//    
//    Orden orden1 = new Orden();
//    orden1.setIdOrden(1);
//    orden1.setFechaOrden(fecha);
//    
//    Orden orden2 = new Orden();
//    orden2.setIdOrden(2);
//    orden2.setFechaOrden(fecha);
//    
//    Orden orden3 = new Orden();
//    orden3.setIdOrden(3);
//    orden3.setFechaOrden(fecha);
//    
//    Orden orden4 = new Orden();
//    orden4.setIdOrden(4);
//    orden4.setFechaOrden(fecha);
////    
//    LineaordenPK lineap1 = new LineaordenPK();
//    lineap1.setIdOrden(1);
//    lineap1.setIdProducto(1);
//    
//    LineaordenPK lineap2 = new LineaordenPK();
//    lineap2.setIdOrden(2);
//    lineap2.setIdProducto(2);
////    
//    Lineaorden linea1 = new Lineaorden();
//    linea1.setCantidad(3);
//    linea1.setOrden(orden1);
//    linea1.setProducto(producto1);
//    linea1.setLineaordenPK(lineap1);
//    
//    Lineaorden linea2 = new Lineaorden();
//    linea2.setCantidad(4);
//    linea2.setOrden(orden2);
//    linea2.setProducto(producto2);
//    linea2.setLineaordenPK(lineap2);
//    
//    
//    try{
//   
//    em.persist(producto1);
//    em.persist(producto2);
//    em.persist(producto3);
//    em.persist(producto4);
//    em.persist(orden1);
//    em.persist(orden2);
//    em.persist(orden3);
//    em.persist(orden4);
//    em.persist(linea1);
//    em.persist(linea2);
//    em.persist(lineap1);
//    em.persist(lineap2);
//    }
//    catch(Exception t){
//        System.out.println("Error en la PERSISTENCIA");
//    }
//    
//    int prod1Id = producto1.getIdProducto();
//    int prod2Id = producto2.getIdProducto();
//    int prod3Id = producto3.getIdProducto();
//    int prod4Id = producto4.getIdProducto();
//   
//        System.out.println("Imprimiendo los Prodcutos");
//    Producto dbproducto1 = em.find(Producto.class, prod1Id);
//        System.out.println("Recuperando datos del Producto uno");
//        System.out.println("\t" + dbproducto1.getIdProducto());
//        System.out.println("\t" + dbproducto1.getDescription());
//        System.out.println("\t" + dbproducto1.getPrecio());
//    
//      Producto dbproducto2 = em.find(Producto.class, prod2Id);
//        System.out.println("Recuperando datos del Producto Dos");
//        System.out.println("\t" + dbproducto2.getIdProducto());
//        System.out.println("\t" + dbproducto2.getDescription());
//        System.out.println("\t" + dbproducto2.getPrecio());   
//    
//    Producto dbproducto3 = em.find(Producto.class, prod3Id);
//        System.out.println("Recuperando datos del Producto Tres");
//        System.out.println("\t" + dbproducto3.getIdProducto());
//        System.out.println("\t" + dbproducto3.getDescription());
//        System.out.println("\t" + dbproducto3.getPrecio()); 
//        
//     Producto dbproducto4 = em.find(Producto.class, prod4Id);
//        System.out.println("Recuperando datos del Producto Cuatro");
//        System.out.println("\t" + dbproducto4.getIdProducto());
//        System.out.println("\t" + dbproducto4.getDescription());
//        System.out.println("\t" + dbproducto4.getPrecio());  
//        
//    int orden1Id = orden1.getIdOrden();
//    int orden2Id = orden2.getIdOrden();
//    int orden3Id = orden3.getIdOrden();
//    int orden4Id = orden4.getIdOrden();
//            
//    Orden dborden1 = em.find(Orden.class, orden1Id);
//        System.out.println("Recuperando datos de la Orden 1");
//        System.out.println("\t" + dborden1.getIdOrden());
//        System.out.println("\t" + dborden1.getFechaOrden());
//          
//    Orden dborden2 = em.find(Orden.class, orden2Id);
//        System.out.println("Recuperando datos de la Orden 2");
//        System.out.println("\t" + dborden2.getIdOrden());
//        System.out.println("\t" + dborden2.getFechaOrden());
//        
//        Orden dborden3 = em.find(Orden.class, orden3Id);
//        System.out.println("Recuperando datos de la Orden 3");
//        System.out.println("\t" + dborden3.getIdOrden());
//        System.out.println("\t" + dborden3.getFechaOrden());
// 
// Orden dborden4 = em.find(Orden.class, orden4Id);
//        System.out.println("Recuperando datos de la Orden 4");
//        System.out.println("\t" + dborden4.getIdOrden());
//        System.out.println("\t" + dborden4.getFechaOrden());
// 
// 
//try{
//    em.getTransaction().commit();
//    }
//    catch(Exception t){
//        System.out.println("Error en el COMMIT");
//    }
//    em.close();
//    emf1.close();
//        
//    }
//    
//    
//}
//
//
